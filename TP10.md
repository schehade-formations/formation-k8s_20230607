# TP 10 - Volumes (emptyDir)

```bash
mkdir ../TP10 && cd ../TP10
kubectl delete all --all
kubectl run my-pod --image=samiche92/my-node-server:1.0.0 --port=8080 -o yaml --dry-run=client > my-pod.yaml
vim my-pod.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: backend
  name: my-pod
spec:
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-ctn-1
    ports:
    - containerPort: 8080
  - image: busybox:glibc
    name: my-ctn-2
    command:
    - sh
    - -c
    - while true; do sleep 1s; done
```

```bash
kubectl apply -f my-pod.yaml 
kubectl get pods
kubectl exec -it my-pod bash
kubectl exec -it -c my-ctn-2 my-pod sh
kubectl exec my-pod -c my-ctn-1 -- ls /tmp
kubectl exec my-pod -c my-ctn-2 -- ls /tmp
kubectl exec my-pod -c my-ctn-1 -- touch /tmp/test1.txt
kubectl exec my-pod -c my-ctn-1 -- ls /tmp
kubectl exec my-pod -c my-ctn-2 -- ls /tmp

vim my-pod.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: backend
  name: my-pod
spec:
  volumes:
  - name: cache-vol
    emptyDir: {}
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-ctn-1
    ports:
    - containerPort: 8080
    volumeMounts:
    - mountPath: /cache1
      name: cache-vol
  - image: busybox:glibc
    name: my-ctn-2
    command:
    - sh
    - -c
    - while true; do sleep 1s; done
    volumeMounts:
    - mountPath: /cache2
      name: cache-vol
```

```bash
kubectl delete -f my-pod.yaml && kubectl apply -f my-pod.yaml
kubectl exec my-pod -c my-ctn-1 -- ls /cache1
kubectl exec my-pod -c my-ctn-2 -- ls /cache2
kubectl exec my-pod -c my-ctn-1 -- touch /cache1/test.txt
kubectl exec my-pod -c my-ctn-2 -- ls /cache2

minikube ssh
docker ps
docker stop k8s_my-ctn-1_my-pod_default_[...]
```
