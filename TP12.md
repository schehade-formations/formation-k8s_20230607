# TP 12 - ConfigMap, Secret

```bash
mkdir ../TP12 && cd ../TP12
kubectl create configmap my-cm --from-literal=delay=5 --from-literal=timeunit=s
kubectl get configmaps
kubectl describe configmap my-cm
kubectl get configmap my-cm -o yaml
vim test.conf
```

```
username=user
password=pwd
```

```bash
kubectl delete cm my-cm
kubectl create configmap my-cm --from-literal=delay=5 --from-literal=timeunit=s --from-file=test.conf
kubectl get cm
kubectl get configmap my-cm -o yaml
kubectl create configmap my-cm-2 --from-env-file=test.conf
kubectl delete cm my-cm
kubectl create configmap my-cm --from-literal=delay=5 --from-literal=timeunit=s --from-file=app-conf=test.conf
kubectl get configmap my-cm -o yaml

cp ../TP08/my-pod.yaml .
vim my-pod.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: backend
    env: dev
  name: my-pod
spec:
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-ctn
    env:
    - name: INTERVAL
      valueFrom:
        configMapKeyRef:
          name: my-cm
          key: delay
    - name: TIME_UNIT
      valueFrom:
        configMapKeyRef:
          name: my-cm
          key: timeunit
    command:
    - sh
    - -c
    - while true; do date; sleep $(INTERVAL)$(TIME_UNIT); done
    ports:
    - containerPort: 8080
```

```bash
kubectl apply -f my-pod.yaml 
kubectl exec my-pod -- ps aux
vim my-pod.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: backend
    env: dev
  name: my-pod
spec:
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-ctn
    envFrom:
    - configMapRef:
        name: my-cm
    command:
    - sh
    - -c
    - while true; do date; sleep $(delay)$(timeunit); done
    ports:
    - containerPort: 8080
```

```bash
kubectl delete -f my-pod.yaml && kubectl apply -f my-pod.yaml
kubectl exec my-pod -- ps aux
kubectl exec my-pod -- env | less

vim my-pod.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: backend
    env: dev
  name: my-pod
spec:
  volumes:
  - name: my-vol
    configMap:
      name: my-cm
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-ctn
    volumeMounts:
    - name: my-vol
      mountPath: /tmp
    envFrom:
    - configMapRef:
        name: my-cm
    command:
    - sh
    - -c
    - while true; do date; sleep $(delay)$(timeunit); done
    ports:
    - containerPort: 8080
```

```bash
kubectl delete -f my-pod.yaml && kubectl apply -f my-pod.yaml 
kubectl exec my-pod -- ls /tmp
kubectl exec my-pod -- cat /tmp/app-conf
kubectl exec my-pod -- cat /tmp/delay
kubectl exec my-pod -- cat /tmp/timeunit

vim my-pod.yaml
```

```yaml
[...]
    volumeMounts:
    - name: my-vol
      subPath: app-conf
      mountPath: /app.conf
[...]
```

```bash
kubectl delete -f my-pod.yaml && kubectl apply -f my-pod.yaml
kubectl exec my-pod -- ls /
kubectl exec my-pod -- cat /app.conf
```

## Secret

```bash
kubectl create secret generic db-sct --from-literal=DB_PWD=rootpwd
kubectl get secrets
kubectl get secret db-sct -o yaml
cp ../TP11/my-db-pod.yaml .
vim my-db-pod.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: db
  name: my-db
spec:
  volumes:
  - name: my-vol
    persistentVolumeClaim:
      claimName: my-pvc-std
  containers:
  - image: mariadb:10.9.5
    name: my-db
    env:
    - name: MARIADB_ROOT_PASSWORD
      valueFrom:
        secretKeyRef:
          name: db-sct
          key: DB_PWD
    volumeMounts:
    - name: my-vol
      mountPath: /var/lib/mysql/
```

```bash
kubectl apply -f my-db-pod.yaml
kubectl exec my-db -- env | grep MARIADB_ROOT_PASSWORD -A 3 -B 3
```
