# TP5 - Deployment

```bash
mkdir ../TP05 && cd ../TP05
kubectl get rs,rc
kubectl delete rs my-rs
kubectl get pods
kubectl create deployment my-app --image=samiche92/my-node-server:1.0.0 -o yaml --dry-run=client > my-deploy.yaml
vim my-deploy.yaml
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-app
spec:
  replicas: 2
  selector:
    matchLabels:
      app: backend
  template:
    metadata:
      labels:
        app: backend
    spec:
      containers:
      - image: samiche92/my-node-server:1.0.0
        name: my-node-server
```yaml

```bash
kubectl apply -f my-deploy.yaml 
kubectl get deployments,replicasets,pods
kubectl get replicasets -w
# Dans un 2nd terminal
kubectl get pods -w
# Dans un 3e terminal
vim my-deploy.yaml
# image de 1.0.0 => 2.0.0
kubectl apply -f my-deploy.yaml
kubectl logs <new-created-pod>

vim app.js
```

```javascript
const http = require('http');
const os = require('os');

var count = 0;

console.log("Server v3 starting...");

var handler = function (request, response) {
  console.log("Received request from " + request.connection.remoteAddress);
  if (request.url == '/repair') {
    count = 0;
    response.writeHead(200);
    response.end("[v3] Server repaired");
    return;
  }
  if (count >= 3) {
    response.writeHead(500);
    response.end("[v3] Some internal error has occurred! Hostname: " + os.hostname() + "\n");
    return;
  }
  count++;
  response.writeHead(200);
  response.end("This is v3 running in " + os.hostname() + "\n");
};

var www = http.createServer(handler);
www.listen(8080);
```

```bash
cp ../TP04/Dockerfile .
docker build -t samiche92/my-node-server:3.0.0 . && docker push samiche92/my-node-server:3.0.0
vim my-deploy.yaml
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-app
  annotations:
    kubernetes.io/change-cause: "v3"
spec:
  replicas: 2
  selector:
    matchLabels:
      app: backend
  template:
    metadata:
      labels:
        app: backend
    spec:
      containers:
      - image: samiche92/my-node-server:3.0.0
        name: my-node-server
```

```bash
kubectl apply -f my-deploy.yaml 
kubectl get pods
kubectl logs my-app-7d586bd99d-fwfhv
kubectl rollout history deployment my-app

kubectl get pods -o wide
kubectl exec my-app-7d586bd99d-fwfhv -- curl 10.244.0.21:8080
kubectl exec my-app-7d586bd99d-fwfhv -- curl 10.244.0.21:8080
kubectl exec my-app-7d586bd99d-fwfhv -- curl 10.244.0.21:8080
kubectl exec my-app-7d586bd99d-fwfhv -- curl 10.244.0.21:8080
kubectl rollout undo deployment my-app --to-revision=2
kubectl get pods
kubectl logs my-app-5d6d5ddc4f-9hdqc
kubectl rollout history deployment my-app
```
